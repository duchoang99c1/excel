package com.example.demo.client;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;


@Service
public class OjbectOkHttp {
    private final OkHttpClient httpClient = new OkHttpClient();

    public String callApiFromProjectTwo(String apiUrl, Map<String, String> queryParams) throws IOException {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(apiUrl).newBuilder();

        if (queryParams != null) {
            for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue());
            }
        }

        Request request = new Request.Builder().url(urlBuilder.build()).build();

        try (Response response = httpClient.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public String callApiFromProjectTwo(String apiUrl) throws IOException {
        return callApiFromProjectTwo(apiUrl, null);
    }
}
