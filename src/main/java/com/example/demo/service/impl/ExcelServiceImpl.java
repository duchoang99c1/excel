package com.example.demo.service.impl;

import com.example.demo.client.OjbectOkHttp;
import com.example.demo.dto.request.AddDataRequest;
import com.example.demo.dto.response.AddDataRespone;
import com.example.demo.entity.ExcelData;
import com.example.demo.repository1.ExcelDataRepository;
import com.example.demo.service.ExcelService;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Service
@Log4j2
public class ExcelServiceImpl implements ExcelService{

    @Autowired
    ExcelDataRepository excelDataRepository;

    @Autowired
    OjbectOkHttp ojbectOkHttp;

    @Override
    public AddDataRespone addData(AddDataRequest request) throws Exception {
        AddDataRespone respone = new AddDataRespone();
        try{
            if (request.getId() == null) {
                throw new Exception("Đầu vào không hợp lệ");
            }
            ExcelData excelData = new ExcelData();
            excelData.setNhafcungcapdichvukyc(request.getNhafcungcapdichvukyc());
            excelData.setCustomerid(request.getCustomerid());
            excelData.setData(request.getData());
            excelData.setAnhgiattomattruoc(request.getAnhgiattomattruoc());
            excelData.setAnhgiattomatsau(request.getAnhgiattomatsau());
            excelData.setAnhchandung(request.getAnhchandung());
            excelData.setKetquaocrnattruoc(request.getKetquaocrnattruoc());
            excelData.setThoigianxuly(request.getThoigianxuly());
            excelData.setKetquaocrmatsau(request.getKetquaocrmatsau());
            excelData.setThoigianxuly1(request.getThoigianxuly1());
            excelData.setKetquasosanhmat(request.getKetquasosanhmat());
            excelData.setThoigianxuly2(request.getThoigianxuly2());
            excelData.setDulieuocrmattruoc(request.getDulieuocrmattruoc());
            excelData.setDulieuocrmatsau(request.getDulieuocrmatsau());
            excelData.setDulieusosanhkhuonmat(request.getDulieusosanhkhuonmat());

            ExcelData excelDataSave = excelDataRepository.save(excelData);
            respone.setExcelData(excelDataSave);
            log.info("END ADD DATA");
            return respone;
        } catch (Exception e){
            log.error(e);
            return respone;
        }
    }

    @Override
    public void writeToExcel() throws Exception {
        try {
            List<ExcelData> excelDataList = excelDataRepository.findAll();

            if (excelDataList.isEmpty()) {
                return;
            }
            try (Workbook workbook = new XSSFWorkbook()) {
                Sheet sheet = workbook.createSheet("Sheet1");

                int rowIndex = 0;
                Row headerRow = sheet.createRow(rowIndex++);
                headerRow.createCell(0).setCellValue("Nhà cung cấp dịch vụ ký số");
                headerRow.createCell(1).setCellValue("Customer ID");
                headerRow.createCell(2).setCellValue("data");
                headerRow.createCell(3).setCellValue("ảnh giấy tờ mặt trước");
                headerRow.createCell(4).setCellValue("ảnh giấy tờ mặt sau");
                headerRow.createCell(5).setCellValue("ảnh chan dung");
                headerRow.createCell(6).setCellValue("kết quả OCR mặt trước");
                headerRow.createCell(7).setCellValue("thời gian xử lý");
                headerRow.createCell(8).setCellValue("kết quả OCR mặt sau");
                headerRow.createCell(9).setCellValue("thời gian xử lý");
                headerRow.createCell(10).setCellValue("kết quả so sánh mặt");
                headerRow.createCell(11).setCellValue("thời gian xử lý");
                headerRow.createCell(12).setCellValue("dữ liệu OCR mặt trước");
                headerRow.createCell(13).setCellValue("dữ liệu OCR mặt sau");
                headerRow.createCell(14).setCellValue("dữ liệu so sánh khuôn mặt");

                for (ExcelData excelData : excelDataList) {
                    Row row = sheet.createRow(rowIndex++);
                    row.createCell(0).setCellValue(excelData.getNhafcungcapdichvukyc());
                    row.createCell(1).setCellValue(excelData.getCustomerid());
                    row.createCell(2).setCellValue(excelData.getData());
                    row.createCell(3).setCellValue(excelData.getAnhgiattomattruoc());
                    row.createCell(4).setCellValue(excelData.getAnhgiattomatsau());
                    row.createCell(5).setCellValue(excelData.getAnhchandung());
                    row.createCell(6).setCellValue(excelData.getKetquaocrnattruoc());
                    row.createCell(7).setCellValue(excelData.getThoigianxuly());
                    row.createCell(8).setCellValue(excelData.getKetquaocrmatsau());
                    row.createCell(9).setCellValue(excelData.getThoigianxuly1());
                    row.createCell(10).setCellValue(excelData.getKetquasosanhmat());
                    row.createCell(11).setCellValue(excelData.getThoigianxuly2());
                    row.createCell(12).setCellValue(excelData.getDulieuocrmattruoc());
                    row.createCell(13).setCellValue(excelData.getDulieuocrmatsau());
                    row.createCell(14).setCellValue(excelData.getDulieusosanhkhuonmat());
                }
                String filePath = "E:\\Project\\mypoi2.xlsx";

                try (FileOutputStream fileOut = new FileOutputStream(filePath)) {
                    workbook.write(fileOut);
                }

                log.info("ghi thành công");
            }

            log.info("thêm excel kết thúc");
        } catch (Exception e) {
            log.error("lỗi ghi vào excel", e);
            throw e;
        }
    }

    @Value("${project.demo2a.api.url}")
    private String readFromExcel;

//    public ExcelServiceImpl(OjbectOkHttp ojbectOkHttp){
//        this.ojbectOkHttp = ojbectOkHttp;
//    }

    public void someMethod() {
        try {
            String apiUrl = readFromExcel + "/endpoint";
            String result = ojbectOkHttp.callApiFromProjectTwo(apiUrl);

            if (result != null && !result.isEmpty()) {
                System.out.println("API : " + result);
            } else {
                System.out.println("không có giá trị API");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

